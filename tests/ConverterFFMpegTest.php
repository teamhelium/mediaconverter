<?php

use helium\media\engines\FFMPEG;
use helium\media\MediaConverter;
use PHPUnit\Framework\TestCase;

class ConverterFFMpegTest extends TestCase {

    private $_inputVideo = null;

    private $_outputDirectory = null;

    protected function setUp() : void {
        $this->_inputVideo = getcwd() . '/tests/SampleVideo.mp4';

        $this->_outputDirectory = getcwd() . '/tests/';

        parent::setUp();
    }

    private function getInstance() {
        $engine = new FFMPEG();

        $converter = new MediaConverter($engine);

        return $converter;
    }

    public function testMp4() {

        $converter = $this->getInstance();

        $output = $this->_outputDirectory . 'output.mp4';

        $returnFile = $converter->convertMp4($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'mp4');
    }

    public function testWebm() {

        $converter = $this->getInstance();

        $output = $this->_outputDirectory . 'output.webm';

        $returnFile = $converter->convertWebm($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'webm');
    }

    public function testMp3() {

        $converter = $this->getInstance();

        $output = $this->_outputDirectory . 'output.mp3';

        $returnFile = $converter->convertMp3($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'mp3');
    }

}