<?php

use helium\media\engines\FFMPEG;
use helium\media\exceptions\FileNotExists;
use helium\media\exceptions\InvalidFormat;
use PHPUnit\Framework\TestCase;

class FFMPEGTest extends TestCase {

    private $_inputVideo = null;

    private $_outputDirectory = null;

    protected function setUp() : void {
        $this->_inputVideo = getcwd() . '/tests/SampleVideo.mp4';

        $this->_outputDirectory = getcwd() . '/tests/';

        parent::setUp();
    }

    public function testFFMPEGLocation() {

        $ffmpeg = new FFMPEG();

        $baseConverter = 'ffmpeg';
        $definedConverter = '/usr/bin/local/ffmpeg';

        $this->assertEquals($baseConverter, $ffmpeg->getFFMPEGLocation());

        $ffmpeg->setFFMPEGLocation($definedConverter);

        $this->assertEquals($definedConverter, $ffmpeg->getFFMPEGLocation());
    }

    public function testFormatOptions() {

        $ffmpeg = new FFMPEG();

        $options = '-vcodec libx264 -acodec aac';

        $this->assertEquals('', $ffmpeg->getFormatOptions('mp4'));

        $ffmpeg->setFormatOptions('mp4', $options);

        $this->assertEquals($options, $ffmpeg->getFormatOptions('mp4'));
    }

    public function testInvalidOption() {

        $ffmpeg = new FFMPEG();

        try {
            $ffmpeg->getFormatOptions('scoobydoo');
            $this->assertTrue(false);
        } catch(InvalidFormat $e) {
            $this->assertTrue(true);
        }

        try {
            $ffmpeg->setFormatOptions('scoobydoo', '-vcodec libx264 -acodec aac');
            $this->assertTrue(false);
        } catch(InvalidFormat $e) {
            $this->assertTrue(true);
        }
    }

    public function testFileNotExist() {

        $ffmpeg = new FFMPEG();

        $output = $this->_outputDirectory . 'newmp4.mp4';

        try {
            $returnFile = $ffmpeg->convertMp4('nothing.mp4', $output);
            $this->assertTrue(false);
        } catch(FileNotExists $e) {
            $this->assertTrue(true);
        }
    }

    public function testConvertMp4ToMp4() {

        $output = $this->_outputDirectory . 'newmp4.mp4';

        $ffmpeg = new FFMPEG();
        $returnFile = $ffmpeg->convertMp4($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'mp4');

    }

    public function testConvertMp4ToOGG() {

        $output = $this->_outputDirectory . 'newogg.ogg';

        $ffmpeg = new FFMPEG();
        $returnFile = $ffmpeg->convertOgg($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'ogg');
    }

    /*
    M3u8 createa multiple files.
    Needs a different testing plan

    public function testConvertMp4ToM3u8() {

        $output = $this->_outputDirectory . 'newm3.m3u8';

        $ffmpeg = new FFMPEG();
        $returnFile = $ffmpeg->convertM3u8($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);

        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'm3u8');
    }*/

    public function testConvertMp4ToWebm() {

        $output = $this->_outputDirectory . 'newwebm.webm';

        $ffmpeg = new FFMPEG();
        $returnFile = $ffmpeg->convertWebm($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);
        
        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'webm');
    }

    public function testConvertMp4ToMp3() {

        $output = $this->_outputDirectory . 'newmp3.mp3';

        $ffmpeg = new FFMPEG();
        $returnFile = $ffmpeg->convertMp3($this->_inputVideo, $output);

        $this->assertFileExists($returnFile);
        unlink($returnFile);
        
        $this->assertEquals($output, $returnFile);

        $file_parts = pathinfo($returnFile);

        $this->assertEquals($file_parts['extension'], 'mp3');
    }
}