<?php

namespace helium\media\interfaces;


interface ConverterInterface {

    public function convertMp4(string $file, string $outputFile = '', $options = []) : string;

    public function convertOgg(string $file, string $outputFile = '', $options = []) : string;

    public function convertWebm(string $file, string $outputFile = '', $options = []) : string;

    public function convertM3u8(string $file , string $outputFile = '', $options = []) : string;

    public function convertMp3(string $file, string $outputFile = '', $options = []) : string;

    public function convertWav(string $file, string $outputFile = '', $options = []) : string;

    public function convertWma(string $file, string $outputFile = '', $options = []) : string;


}