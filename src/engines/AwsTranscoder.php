<?php
namespace helium\media\engines;

use helium\media\interfaces\ConverterInterface;

/**
 * @todo Figure out how to use Amazons Transcoding Services
 * 
 * https://docs.aws.amazon.com/elastictranscoder/latest/developerguide/sample-code.html#php-sample
 * 
 */
class AwsTranscoder implements ConverterInterface {

    public function convertMp4(string $file, string $outputFile = '', $options = []) {

    }

    public function convertOgg(string $file, string $outputFile = '', $options = []) {

    }

    public function convertWebm(string $file, string $outputFile = '', $options = []) {
        
    }

    public function convertM3u8(string $file , string $outputFile = '', $options = []) {
        
    }

    public function convertMp3(string $file, string $outputFile = '', $options = []) {

    }

    public function convertWav(string $file, string $outputFile = '', $options = []) {

    }

    public function convertWma(string $file, string $outputFile = '', $options = []) {

    }

}