<?php

namespace helium\media\engines;

use helium\media\exceptions\ConversionError;
use helium\media\exceptions\FileNotExists;
use helium\media\exceptions\InvalidFormat;
use helium\media\interfaces\ConverterInterface;

class FFMPEG implements ConverterInterface {

    private $_ffmpeg = 'ffmpeg';

    private $_converterOptions = [
        'mp4' => '',
        'ogg' => '',
        'webm' => '',
        'm3u8' => '',
        'mp3' => '',
        'wav' => '',
        'wma' => '',
    ];

    private $_extensionOptions = [
        'mp4' => 'mp4',
        'ogg' => 'ogg',
        'webm' => 'webm',
        'm3u8' => 'm3u8',
        'mp3' => 'mp3',
        'wav' => 'wav',
        'wma' => 'wma',
    ];

    public function convertMp4(string $inputFile, string $outputFile = '', $options = []) : string {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['mp4']);
    }

    public function convertOgg(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['ogg']);
    }

    public function convertWebm(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['webm']);
    }

    public function convertM3u8(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['m3u8']);
    }

    public function convertMp3(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['mp3']);
    }

    public function convertWav(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['wav']);
    }

    public function convertWma(string $inputFile, string $outputFile = '', $options = []) : string  {
        return $this->_convertFile($inputFile, $outputFile, $this->_extensionOptions['wma']);
    }

    /**
     * Sets the location of the ffmpeg application on the server. This should be used
     * if the default ffmpeg is not available on the command line. For example, define 
     * it as '/usr/bin/local/ffmpeg'
     * 
     * @param string $ffmpegPath
     * 
     * @return void
     */
    public function setFFMPEGLocation(string $ffmpegPath) {
        $this ->_ffmpeg = $ffmpegPath;
    }

    public function getFFMPEGLocation() {
        return $this ->_ffmpeg;
    }

    /**
     * Sets the options that we can be used for converting a file with
     * ffmpeg per the specific format. For example, setting '-g 60 -hls_time 2'
     * will produce ffmpeg -i input -g 60 -hls_time 2 out .
     * 
     * @param string $format One of the supported formats
     * @param string $options The command line options
     * 
     * @return void
     */
    public function setFormatOptions(string $format, string $options) {

        if(!isset($this->_converterOptions[$format])) {
            throw new InvalidFormat("Format {$format} is not supported.");
        }

        $this->_converterOptions[$format] = $options;
    }

    public function getFormatOptions(string $format) {
        if(!isset($this->_converterOptions[$format])) {
            throw new InvalidFormat("Format {$format} is not supported.");
        }

        return $this->_converterOptions[$format];
    }

    private function _fileExistCheck(string $file) {

        if(!file_exists($file)) {
            throw new FileNotExists("File {$file} does not exist.");
        }
    }

    /**
     * If an output file has not been specifed,a temporary one
     * will be created.
     * 
     * @param string $outputFile
     * @param string $extension And extension for the file, ie mp4
     */
    private function _generateOutputFile(string $outputFile, $extension) {

        //Ensure the output file has the correct extension
        $outputFile = $this->_applyExtension($outputFile, $extension);

        //If the output file has been set, return else
        if($outputFile) {
            return $outputFile;
        }

        $filename = uniqid() . $extension;

        $file_location = tempnam(sys_get_temp_dir(), $filename);

        return $file_location;
    }

    /**
     * Will add the extension is the correct one does not exist. This
     * is required for ffmpeg to correctly convert.
     * 
     * @param string $outputFile The file that will be generated
     * @param string $extension The desired extension
     */
    private function _applyExtension(string $outputFile, $extension) {

        $file_parts = pathinfo($outputFile);

        if($file_parts['extension'] != $extension) {
            return $outputFile .= '.' . $extension;
        }

        return $outputFile;
    }

    /**
     * Checks to see if the conversion was completed and is correct
     */
    private function _checkConversion($outputFile) {

        if(!file_exists($outputFile)) {
            throw new ConversionError("There was an error converting {$outputFile}.");
        }
    }

    private function _convertFile($inputFile, $outputFile, $extension) {
        $this->_fileExistCheck($inputFile);
        
        $outputFile = $this->_generateOutputFile($outputFile, $this->_extensionOptions[$extension]); 
        
        $this->_transcode($inputFile, $this->_converterOptions[$extension], $outputFile);

        $this->_checkConversion($outputFile);

        return $outputFile;
    }

    private function _transcode(string $inputFile, string $outputFile, string $options = '') {

        exec($this->_ffmpeg .' -i '  . $inputFile . ' ' . $options . ' ' . $outputFile);
    }

    
}