<?php
namespace helium\media;

use helium\media\interfaces\ConverterInterface;

class MediaConverter implements ConverterInterface {

    private $_conveter = null;

    public function __construct(ConverterInterface $converter)  {
        $this->_conveter = $converter;
    }

    public function convertMp4($file, string $outputFile = '', $options = [])  : string   {
        return $this->_conveter->convertMp4($file, $outputFile, $options);
    }

    public function convertOgg(string $file, string $outputFile = '', $options = [])  : string  {
        return $this->_conveter->convertOgg($file, $outputFile, $options);
    }

    public function convertWebm(string $file, string $outputFile = '', $options = [])  : string  {
        return $this->_conveter->convertWebm($file, $outputFile, $options);
    }

    public function convertM3u8(string $file , string $outputFile = '', $options = []) : string  {
        return $this->_conveter->convertM3u8($file, $outputFile, $options);
    }

    public function convertMp3(string $file, string $outputFile = '', $options = []) : string  {
        return $this->_conveter->convertMp3($file, $outputFile, $options);
    }

    public function convertWav(string $file, string $outputFile = '', $options = []) : string  {
        return $this->_conveter->convertWav($file, $outputFile, $options);
    }

    public function convertWma(string $file, string $outputFile = '', $options = []) : string  {
        return $this->_conveter->convertWma($file, $outputFile, $options);
    }

}