# MediaConverter

The MediaConverter is a class that is used to convert the media files (video/audio) into different
formats. The class is designed to work with several different conversion tools such as ffmpeg
and AWS Transcoding services.

## How To Use

The class is designed with several engines that will allow you change how the developer will
transcode the files. The goal of each engine is abstract as many complexities possible.

#### FFMPEG Example

```
use helium\media\engines\FFMPEG;
use helium\media\MediaConverter;

$engine = new FFMPEG();
$engine->setFFMPEGLocation('/usr/local/bin/ffmpeg');
$engine->setFormatOptions('mp4', '-vcodec libx264 -acodec aac');

$converter = new MediaConverter($engine);

$fileToConvert = 'fileToConvert.ogg';
$saveFileLocation = 'newFile.mp4';

$converter->convertMp4($fileToConvert, $saveFileLocation);

//After a successful conversion, save the new file
//to a model, place in cloud storage, etc.
```